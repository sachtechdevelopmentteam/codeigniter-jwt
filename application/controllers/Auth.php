<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

/*
 * Changes:
 * 1. This project contains .htaccess file for windows machine.
 *    Please update as per your requirements.
 *    Samples (Win/Linux): http://stackoverflow.com/questions/28525870/removing-index-php-from-url-in-codeigniter-on-mandriva
 *
 * 2. Change 'encryption_key' in application\config\config.php
 *    Link for encryption_key: http://jeffreybarke.net/tools/codeigniter-encryption-key-generator/
 * 
 * 3. Change 'jwt_key' in application\config\jwt.php
 *
 */

class Auth extends REST_Controller
{
    /**
     * URL: http://localhost/CodeIgniter-JWT-Sample/auth/token
     * Method: GET
     */
    public function token_get()   
    {
        $tokenData = array();
        $tokenData['id'] = 1; //TODO: Replace with data for token
        $output['token'] = AUTHORIZATION::generateToken($tokenData);
        $this->set_response($output, REST_Controller::HTTP_OK);
    }

    /**
     * URL: http://localhost/CodeIgniter-JWT-Sample/auth/token
     * Method: POST
     * Header Key: Authorization
     * Value: Auth token generated in GET call
     */
    public function token_post()
    {
        $headers = $this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {

            $jwt = $headers['Authorization'];

            if (strpos($jwt, 'Bearer') !== false) {

                $jwt1 = explode('Bearer',$jwt);
                $jwt_ =  trim($jwt1[1]);
    
    
            }
            else{
                echo 'Nothing to split';
            }
            //TODO: Change 'token_timeout' in application\config\jwt.php
            $decodedToken = AUTHORIZATION::validateTimestamp($jwt_);

            // return response if token is valid
            if ($decodedToken != false) {
                $this->set_response($decodedToken, REST_Controller::HTTP_OK);
                return;
            }
        }

        $this->set_response("Time Out", REST_Controller::HTTP_UNAUTHORIZED);
    }



    public function insert_post(){

        $user_id     = trim($this->post('user_id'));
        $user_email     = trim($this->post('user_email'));
        $user_type     = trim($this->post('user_type'));

        $data = array(
            'user_id'=>  $user_id,
            'user_email'=>  $user_email,
            'user_type'=> $user_type,
            'timestamp'=>now()
        );

        $output['token'] = AUTHORIZATION::generateToken($data);
        $this->set_response($output, REST_Controller::HTTP_OK);
    }
}